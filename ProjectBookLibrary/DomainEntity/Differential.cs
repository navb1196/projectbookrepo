﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class Differential
    {
        public long Id { get; set; }
        public string RefId { get; set; }
        public string Type { get; set; }
        public string Mid { get; set; }
        public DateTime EntryDate { get; set; }
        public string Details { get; set; }
    }
}
