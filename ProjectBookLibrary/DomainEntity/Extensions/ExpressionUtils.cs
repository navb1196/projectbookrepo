﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace OneSystem.Domain.Models
{
    public static class ExpressionUtils
    {
        public static Expression<Func<TOuter, bool>> Any<TOuter, TInner>(this Expression<Func<TOuter, IEnumerable<TInner>>> innerSelector, Expression<Func<TInner, bool>> innerPredicate)
        {
            var parameter = innerSelector.Parameters[0];
            var body = Expression.Call(
                typeof(Enumerable), nameof(Enumerable.Any), new[] { typeof(TInner) },
                innerSelector.Body, innerPredicate);
            return Expression.Lambda<Func<TOuter, bool>>(body, parameter);
        }
        public static Expression<Func<TOuter, bool>> All<TOuter, TInner>(this Expression<Func<TOuter, IEnumerable<TInner>>> innerSelector, Expression<Func<TInner, bool>> innerPredicate)
        {
            var parameter = innerSelector.Parameters[0];
            var body = Expression.Call(
                typeof(Enumerable), nameof(Enumerable.All), new[] { typeof(TInner) },
                innerSelector.Body, innerPredicate);
            return Expression.Lambda<Func<TOuter, bool>>(body, parameter);
        }
        public static Expression<Func<TOuter, bool>> NotAny<TOuter, TInner>(this Expression<Func<TOuter, IEnumerable<TInner>>> innerSelector, Expression<Func<TInner, bool>> innerPredicate)
        {
            var parameter = innerSelector.Parameters[0];
            var body = Expression.Not(Expression.Call(
                typeof(Enumerable), nameof(Enumerable.Any), new[] { typeof(TInner) },
                innerSelector.Body, innerPredicate));
            return Expression.Lambda<Func<TOuter, bool>>(body, parameter);
        }
    }
}
