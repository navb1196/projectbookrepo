﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class OneSystemDbContext : DbContext
    {
        public OneSystemDbContext()
        {
        }

        public OneSystemDbContext(DbContextOptions<OneSystemDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Differential> Differentials { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductArt> ProductArts { get; set; }
        public virtual DbSet<ProductAsset> ProductAssets { get; set; }
        public virtual DbSet<ProductAttribute> ProductAttributes { get; set; }
        public virtual DbSet<ProductColor> ProductColors { get; set; }
        public virtual DbSet<ProductCopy> ProductCopies { get; set; }
        public virtual DbSet<ProductPrice> ProductPrices { get; set; }
        public virtual DbSet<ProductProduct> ProductProducts { get; set; }
        public virtual DbSet<ProductProductArtAttribute> ProductProductArtAttributes { get; set; }
        public virtual DbSet<ProductProductAsset> ProductProductAssets { get; set; }
        public virtual DbSet<ProductProductAssetAttribute> ProductProductAssetAttributes { get; set; }
        public virtual DbSet<ProductProductAttribute> ProductProductAttributes { get; set; }
        public virtual DbSet<ProductProductAttributeAudit> ProductProductAttributeAudits { get; set; }
        public virtual DbSet<ProductProductAttributeProductAttribute> ProductProductAttributeProductAttributes { get; set; }
        public virtual DbSet<ProductProductAttributeProductRule> ProductProductAttributeProductRules { get; set; }
        public virtual DbSet<ProductProductPrice> ProductProductPrices { get; set; }
        public virtual DbSet<ProductProductProductAttributeProductProductAsset> ProductProductProductAttributeProductProductAssets { get; set; }
        public virtual DbSet<ProductProductRule> ProductProductRules { get; set; }
        public virtual DbSet<ProductProductRuleProductRule> ProductProductRuleProductRules { get; set; }
        public virtual DbSet<ProductPurchaseOrder> ProductPurchaseOrders { get; set; }
        public virtual DbSet<ProductRule> ProductRules { get; set; }
        public virtual DbSet<ProductSku> ProductSkus { get; set; }
        public virtual DbSet<ProductSkuPrefixSuffix> ProductSkuPrefixSuffixes { get; set; }
        public virtual DbSet<ProductSkuPrefixSuffixAttribute> ProductSkuPrefixSuffixAttributes { get; set; }
        public virtual DbSet<ProductStaticTypeReference> ProductStaticTypeReferences { get; set; }
        public virtual DbSet<ProductSupplierProduct> ProductSupplierProducts { get; set; }
        public virtual DbSet<ProvisionMarkerDss> ProvisionMarkerDsses { get; set; }
        public virtual DbSet<SchemaInfoDss> SchemaInfoDsses { get; set; }
        public virtual DbSet<ScopeConfigDss> ScopeConfigDsses { get; set; }
        public virtual DbSet<ScopeInfoDss> ScopeInfoDsses { get; set; }
        public virtual DbSet<Supplier> Suppliers { get; set; }
        public virtual DbSet<SupplierAttribute> SupplierAttributes { get; set; }
        public virtual DbSet<SupplierMediaAttribute> SupplierMediaAttributes { get; set; }
        public virtual DbSet<SupplierMedium> SupplierMedia { get; set; }
        public virtual DbSet<SupplierOrder> SupplierOrders { get; set; }
        public virtual DbSet<SupplierOrderAddress> SupplierOrderAddresses { get; set; }
        public virtual DbSet<SupplierOrderShipment> SupplierOrderShipments { get; set; }
        public virtual DbSet<SupplierOrderShipmentProduct> SupplierOrderShipmentProducts { get; set; }
        public virtual DbSet<SupplierProduct> SupplierProducts { get; set; }
        public virtual DbSet<SupplierProductAttribute> SupplierProductAttributes { get; set; }
        public virtual DbSet<SupplierProductColor> SupplierProductColors { get; set; }
        public virtual DbSet<SupplierProductDetail> SupplierProductDetails { get; set; }
        public virtual DbSet<SupplierProductDetailFob> SupplierProductDetailFobs { get; set; }
        public virtual DbSet<SupplierProductDetailFobArray> SupplierProductDetailFobArrays { get; set; }
        public virtual DbSet<SupplierProductDetailFobcurrencySupported> SupplierProductDetailFobcurrencySupporteds { get; set; }
        public virtual DbSet<SupplierProductDetailFobproduct> SupplierProductDetailFobproducts { get; set; }
        public virtual DbSet<SupplierProductDetailLocationArray> SupplierProductDetailLocationArrays { get; set; }
        public virtual DbSet<SupplierProductDetailLocationDecorationArray> SupplierProductDetailLocationDecorationArrays { get; set; }
        public virtual DbSet<SupplierProductDetailLocationDecorationCharge> SupplierProductDetailLocationDecorationCharges { get; set; }
        public virtual DbSet<SupplierProductDetailLocationDecorationChargePrice> SupplierProductDetailLocationDecorationChargePrices { get; set; }
        public virtual DbSet<SupplierProductDetailPartArray> SupplierProductDetailPartArrays { get; set; }
        public virtual DbSet<SupplierProductDetailPartPriceArray> SupplierProductDetailPartPriceArrays { get; set; }
        public virtual DbSet<SupplierProductInventory> SupplierProductInventories { get; set; }
        public virtual DbSet<SupplierProductInventoryFuture> SupplierProductInventoryFutures { get; set; }
        public virtual DbSet<SupplierProductInventoryLocation> SupplierProductInventoryLocations { get; set; }
        public virtual DbSet<SupplierProductPrice> SupplierProductPrices { get; set; }
        public virtual DbSet<SupplierProductSupplierProductAttribute> SupplierProductSupplierProductAttributes { get; set; }
        public virtual DbSet<SystemMetaProduct> SystemMetaProducts { get; set; }
        public virtual DbSet<SystemQueue> SystemQueues { get; set; }
        public virtual DbSet<SystemQueueUser> SystemQueueUsers { get; set; }
        public virtual DbSet<Workflow> Workflows { get; set; }
        public virtual DbSet<WorkflowTask> WorkflowTasks { get; set; }
        public virtual DbSet<WorkflowTaskAttribute> WorkflowTaskAttributes { get; set; }
        public virtual DbSet<WorkflowTaskDependency> WorkflowTaskDependencies { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=pos-web-server2.positive.local;Database=OneSystemDb;Trusted_Connection=false; User ID=svam;Password=svam@pos;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Differential>(entity =>
            {
                entity.ToTable("Differential");

                entity.Property(e => e.Details).HasMaxLength(4000);

                entity.Property(e => e.EntryDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Mid)
                    .IsRequired()
                    .HasMaxLength(24)
                    .HasColumnName("MID");

                entity.Property(e => e.RefId)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.ToTable("Product");

                entity.HasIndex(e => e.LastModifiedDate, "IX_Product_LastModifiedDate");

                entity.HasIndex(e => e.LastPimsyncDate, "IX_Product_LastPIMSyncDate");

                entity.HasIndex(e => e.Sku, "IX_Product_Sku");

                entity.HasIndex(e => new { e.Sku, e.Source }, "IX_Product_SkuSource")
                    .IsUnique();

                entity.Property(e => e.EntryDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsActive)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.IsSyncEnabled)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.LastModifiedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastPimsyncDate).HasColumnName("LastPIMSyncDate");

                entity.Property(e => e.LastRuleDate).HasColumnType("datetime");

                entity.Property(e => e.LastUsername).HasMaxLength(50);

                entity.Property(e => e.Mid)
                    .HasMaxLength(24)
                    .HasColumnName("MID");

                entity.Property(e => e.Sku)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Source).HasMaxLength(20);

                entity.Property(e => e.Username).HasMaxLength(50);

                entity.HasOne(d => d.SkuNavigation)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.SkuId)
                    .HasConstraintName("FK_Product_ProductSku");
            });

            modelBuilder.Entity<ProductArt>(entity =>
            {
                entity.ToTable("Product_Art");

                entity.HasIndex(e => new { e.ParentCategory, e.ChildCategory, e.MetaId }, "IX_Product_Art_Categories")
                    .IsUnique();

                entity.Property(e => e.ChildCategory).HasMaxLength(50);

                entity.Property(e => e.ImageUrl).HasMaxLength(250);

                entity.Property(e => e.Keywords).HasMaxLength(2000);

                entity.Property(e => e.MetaId).HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.ParentCategory).HasMaxLength(50);

                entity.Property(e => e.SvgUrl).HasMaxLength(250);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<ProductAsset>(entity =>
            {
                entity.ToTable("Product_Asset");

                entity.Property(e => e.AccessLevel).HasMaxLength(50);

                entity.Property(e => e.Hash)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.S3bucketName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("S3BucketName");

                entity.Property(e => e.S3id)
                    .IsRequired()
                    .HasMaxLength(32)
                    .HasColumnName("S3Id");

                entity.Property(e => e.Source).HasMaxLength(1000);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<ProductAttribute>(entity =>
            {
                entity.ToTable("Product_Attribute");

                entity.HasIndex(e => e.AuditId, "IX_Product_Attribute_AuditId");

                entity.HasIndex(e => e.ColorId, "IX_Product_Attribute_ColorId");

                entity.HasIndex(e => e.Name, "IX_Product_Attribute_Name");

                entity.HasIndex(e => new { e.Type, e.ParentAttributeId, e.Name }, "IX_Product_Attribute_NameParentType");

                entity.HasIndex(e => e.ParentAttributeId, "IX_Product_Attribute_ParentAttributeId");

                entity.HasIndex(e => e.Type, "IX_Product_Attribute_Type");

                entity.HasIndex(e => e.Value, "IX_Product_Attribute_Value");

                entity.Property(e => e.EntryDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Hiearchy).HasMaxLength(4000);

                entity.Property(e => e.HiearchyString).HasMaxLength(4000);

                entity.Property(e => e.LastModifiedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastUsername).HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.Type).HasMaxLength(250);

                entity.Property(e => e.Username).HasMaxLength(50);

                entity.Property(e => e.Value).HasMaxLength(1000);

                entity.HasOne(d => d.Audit)
                    .WithMany(p => p.ProductAttributes)
                    .HasForeignKey(d => d.AuditId)
                    .HasConstraintName("FK_Product_Attribute_ProductAttributeAudit");

                entity.HasOne(d => d.Color)
                    .WithMany(p => p.ProductAttributes)
                    .HasForeignKey(d => d.ColorId)
                    .HasConstraintName("FK_Product_Attribute_ProductColor");

                entity.HasOne(d => d.ParentAttribute)
                    .WithMany(p => p.InverseParentAttribute)
                    .HasForeignKey(d => d.ParentAttributeId)
                    .HasConstraintName("FK_Product_Attribute_Product_Attribute");
            });

            modelBuilder.Entity<ProductColor>(entity =>
            {
                entity.ToTable("Product_Color");

                entity.HasIndex(e => new { e.Pms, e.Hex }, "IX_Product_Color_PMSHex")
                    .IsUnique();

                entity.Property(e => e.Hex)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.IsActive)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.LastModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(250);

                entity.Property(e => e.Pms)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("PMS");

                entity.Property(e => e.SwatchId).HasMaxLength(250);
            });

            modelBuilder.Entity<ProductCopy>(entity =>
            {
                entity.ToTable("Product_Copy");

                entity.HasIndex(e => new { e.ProductId, e.Name, e.Type }, "IX_Product_Copy_1")
                    .IsUnique();

                entity.HasIndex(e => e.Name, "IX_Product_Copy_Name");

                entity.HasIndex(e => e.ProductId, "IX_Product_Copy_ProductId");

                entity.HasIndex(e => e.Type, "IX_Product_Copy_Type");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Value).HasMaxLength(4000);

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductCopies)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Product_Copy_Product");
            });

            modelBuilder.Entity<ProductPrice>(entity =>
            {
                entity.ToTable("Product_Price");

                entity.HasIndex(e => e.Type, "IX_Product_Price_Type");

                entity.Property(e => e.Discount).HasColumnType("decimal(18, 6)");

                entity.Property(e => e.DiscountType).HasMaxLength(250);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.TypeMeta).HasMaxLength(2000);

                entity.Property(e => e.Value).HasColumnType("decimal(18, 6)");
            });

            modelBuilder.Entity<ProductProduct>(entity =>
            {
                entity.HasKey(e => new { e.ChildProductId, e.ParentProductId })
                    .HasName("PK__Product___1C67D6E2D5C1A645");

                entity.ToTable("Product_Product");

                entity.HasIndex(e => e.ChildProductId, "IX_Product_Product_ChildProductId");

                entity.HasIndex(e => e.ParentProductId, "IX_Product_Product_ParentProductId");

                entity.Property(e => e.Hiearchy).HasMaxLength(4000);

                entity.Property(e => e.Type).HasMaxLength(50);

                entity.Property(e => e.Uom)
                    .HasColumnType("decimal(18, 6)")
                    .HasColumnName("UOM");

                entity.HasOne(d => d.ChildProduct)
                    .WithMany(p => p.ProductProductChildProducts)
                    .HasForeignKey(d => d.ChildProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductProduct_ProductChild");

                entity.HasOne(d => d.ParentProduct)
                    .WithMany(p => p.ProductProductParentProducts)
                    .HasForeignKey(d => d.ParentProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductProduct_ProductParent");
            });

            modelBuilder.Entity<ProductProductArtAttribute>(entity =>
            {
                entity.ToTable("Product_ProductArtAttribute");

                entity.HasIndex(e => e.ArtId, "IX_Product_ProductArtAttribute_ArtId");

                entity.HasIndex(e => e.LastModifiedDate, "IX_Product_ProductArtAttribute_LastModifiedDate");

                entity.HasIndex(e => e.Name, "IX_Product_ProductArtAttribute_Name");

                entity.HasIndex(e => new { e.Name, e.Value }, "IX_Product_ProductArtAttribute_NameValue");

                entity.HasIndex(e => e.Type, "IX_Product_ProductArtAttribute_Type");

                entity.HasIndex(e => e.Value, "IX_Product_ProductArtAttribute_Value");

                entity.Property(e => e.EntryDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastModifiedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastUsername).HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.Username).HasMaxLength(50);

                entity.Property(e => e.Value).HasMaxLength(1000);

                entity.HasOne(d => d.Art)
                    .WithMany(p => p.ProductProductArtAttributes)
                    .HasForeignKey(d => d.ArtId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Product_ProductArtAttribute_Product_Art");
            });

            modelBuilder.Entity<ProductProductAsset>(entity =>
            {
                entity.ToTable("Product_ProductAsset");

                entity.HasIndex(e => e.AssetId, "IX_Product_ProductAsset_AssetId");

                entity.HasIndex(e => e.ProductId, "IX_Product_ProductAsset_ProductId");

                entity.Property(e => e.Description).HasMaxLength(2000);

                entity.Property(e => e.IsHeritable).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsInherited).HasDefaultValueSql("((0))");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.HasOne(d => d.Asset)
                    .WithMany(p => p.ProductProductAssets)
                    .HasForeignKey(d => d.AssetId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductProductAsset_Asset");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductProductAssets)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductProductAsset_Product");
            });

            modelBuilder.Entity<ProductProductAssetAttribute>(entity =>
            {
                entity.ToTable("Product_ProductAssetAttribute");

                entity.HasIndex(e => e.AssetId, "IX_Product_ProductAssetAttribute_AssetId");

                entity.HasIndex(e => e.LastModifiedDate, "IX_Product_ProductAssetAttribute_LastModifiedDate");

                entity.HasIndex(e => e.Name, "IX_Product_ProductAssetAttribute_Name");

                entity.HasIndex(e => new { e.Name, e.Value }, "IX_Product_ProductAssetAttribute_NameValue");

                entity.HasIndex(e => e.Type, "IX_Product_ProductAssetAttribute_Type");

                entity.HasIndex(e => e.Value, "IX_Product_ProductAssetAttribute_Value");

                entity.Property(e => e.EntryDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastModifiedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastUsername).HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.Username).HasMaxLength(50);

                entity.Property(e => e.Value).HasMaxLength(1000);

                entity.HasOne(d => d.Asset)
                    .WithMany(p => p.ProductProductAssetAttributes)
                    .HasForeignKey(d => d.AssetId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Product_ProductAssetAttribute_Product_Asset");
            });

            modelBuilder.Entity<ProductProductAttribute>(entity =>
            {
                entity.ToTable("Product_ProductAttribute");

                entity.HasIndex(e => e.AttributeId, "IX_Product_ProductAttribute_AttributeId");

                entity.HasIndex(e => e.ProductId, "IX_Product_ProductAttribute_ProductId");

                entity.HasIndex(e => new { e.ProductId, e.AttributeId }, "IX_Product_ProductAttribute_ProductIdAttributeId");

                entity.HasOne(d => d.Attribute)
                    .WithMany(p => p.ProductProductAttributes)
                    .HasForeignKey(d => d.AttributeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductAttribute_Product_Attribute");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductProductAttributes)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductAttribute_Product");
            });

            modelBuilder.Entity<ProductProductAttributeAudit>(entity =>
            {
                entity.ToTable("Product_ProductAttribute_Audit");

                entity.HasIndex(e => e.Type, "IX_Product_ProductAttribute_Audit_TypeValue");

                entity.Property(e => e.DataType).HasMaxLength(50);

                entity.Property(e => e.EntryDate).HasColumnType("datetime");

                entity.Property(e => e.LastValidationDate).HasColumnType("datetime");

                entity.Property(e => e.Type).HasMaxLength(50);

                entity.Property(e => e.Value).HasMaxLength(2000);
            });

            modelBuilder.Entity<ProductProductAttributeProductAttribute>(entity =>
            {
                entity.HasKey(e => new { e.Type, e.AttributeId, e.AttributeIdRef })
                    .HasName("PK__Product___16468FBFA8A6F2D1");

                entity.ToTable("Product_ProductAttribute_ProductAttribute");

                entity.Property(e => e.Type).HasMaxLength(100);

                entity.Property(e => e.Value).HasMaxLength(250);

                entity.HasOne(d => d.Attribute)
                    .WithMany(p => p.ProductProductAttributeProductAttributeAttributes)
                    .HasForeignKey(d => d.AttributeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductAttribute_ProductAttribute");

                entity.HasOne(d => d.AttributeIdRefNavigation)
                    .WithMany(p => p.ProductProductAttributeProductAttributeAttributeIdRefNavigations)
                    .HasForeignKey(d => d.AttributeIdRef)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Product_ProductAttribute_ProductAttribute_Attribute");
            });

            modelBuilder.Entity<ProductProductAttributeProductRule>(entity =>
            {
                entity.HasKey(e => new { e.RuleId, e.AttributeId })
                    .HasName("PK__Product___AD1CCA7CA6E79D7A");

                entity.ToTable("Product_ProductAttribute_ProductRule");

                entity.HasOne(d => d.Attribute)
                    .WithMany(p => p.ProductProductAttributeProductRules)
                    .HasForeignKey(d => d.AttributeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Product_ProductAttribute_ProductRule_ProducAttribute");

                entity.HasOne(d => d.Rule)
                    .WithMany(p => p.ProductProductAttributeProductRules)
                    .HasForeignKey(d => d.RuleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Product_ProductAttribute_ProductRule_ProductRule");
            });

            modelBuilder.Entity<ProductProductPrice>(entity =>
            {
                entity.ToTable("Product_ProductPrice");

                entity.HasIndex(e => new { e.Division, e.PriceId, e.EntryDate, e.ProductId }, "IX_Product_ProductPrice")
                    .IsUnique();

                entity.HasIndex(e => e.PriceId, "IX_Product_ProductPrice_PriceId");

                entity.HasIndex(e => e.ProductId, "IX_Product_ProductPrice_ProductId");

                entity.Property(e => e.Division)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.EffectiveDate).HasColumnType("date");

                entity.Property(e => e.EntryDate).HasColumnType("datetime");

                entity.Property(e => e.ExpireDate).HasColumnType("date");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductProductPrices)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Product_ProductPrice_Product");
            });

            modelBuilder.Entity<ProductProductProductAttributeProductProductAsset>(entity =>
            {
                entity.ToTable("Product_ProductProductAttribute_ProductProductAsset");

                entity.HasOne(d => d.ProductProductAsset)
                    .WithMany(p => p.ProductProductProductAttributeProductProductAssets)
                    .HasForeignKey(d => d.ProductProductAssetId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Product_ProductProductAttributeProductProductAsset_ProductProductAsset");

                entity.HasOne(d => d.ProductProductAttribute)
                    .WithMany(p => p.ProductProductProductAttributeProductProductAssets)
                    .HasForeignKey(d => d.ProductProductAttributeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Product_ProductProductAttributeProductProductAsset_ProductProductAttribute");
            });

            modelBuilder.Entity<ProductProductRule>(entity =>
            {
                entity.ToTable("Product_ProductRule");

                entity.HasIndex(e => new { e.ProductId, e.RuleId }, "IX_Product_ProductRule_ProductIdRuleId");

                entity.Property(e => e.EntryDate).HasColumnType("datetime");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.Reason).HasMaxLength(2000);

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductProductRules)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Product_ProductRule_Product");

                entity.HasOne(d => d.Rule)
                    .WithMany(p => p.ProductProductRules)
                    .HasForeignKey(d => d.RuleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Product_ProductRule_ProductRule");
            });

            modelBuilder.Entity<ProductProductRuleProductRule>(entity =>
            {
                entity.ToTable("Product_ProductRule_ProductRule");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<ProductPurchaseOrder>(entity =>
            {
                entity.HasKey(e => new { e.Ponumber, e.LineNumber })
                    .HasName("PK__Product___2026CA308E040ADE");

                entity.ToTable("Product_PurchaseOrder");

                entity.Property(e => e.Ponumber)
                    .HasMaxLength(50)
                    .HasColumnName("PONumber");

                entity.Property(e => e.Available).HasColumnType("date");

                entity.Property(e => e.EntryDate).HasColumnType("datetime");

                entity.Property(e => e.Received).HasDefaultValueSql("((0))");

                entity.Property(e => e.RequiredDate).HasColumnType("date");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductPurchaseOrders)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductPurchaseOrder_Product");
            });

            modelBuilder.Entity<ProductRule>(entity =>
            {
                entity.ToTable("Product_Rule");

                entity.HasIndex(e => new { e.Source, e.Type, e.Name }, "IX_Product_Rule_SourceTypeName")
                    .IsUnique();

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.LastModifiedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Source)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<ProductSku>(entity =>
            {
                entity.ToTable("Product_Sku");

                entity.HasIndex(e => new { e.IncrementRef, e.PrefixId, e.SuffixId }, "IX_Product_PerfixSuffixIncrement")
                    .IsUnique();

                entity.Property(e => e.EntryDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastModifiedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastUsername).HasMaxLength(50);

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.Property(e => e.Username).HasMaxLength(50);

                entity.HasOne(d => d.Prefix)
                    .WithMany(p => p.ProductSkuPrefixes)
                    .HasForeignKey(d => d.PrefixId)
                    .HasConstraintName("FK_Product_Sku_ProductSkuPrefixSuffix");

                entity.HasOne(d => d.Suffix)
                    .WithMany(p => p.ProductSkuSuffixes)
                    .HasForeignKey(d => d.SuffixId)
                    .HasConstraintName("FK_Product_Sku_ProductSkuPrefixSuffix1");
            });

            modelBuilder.Entity<ProductSkuPrefixSuffix>(entity =>
            {
                entity.ToTable("Product_Sku_PrefixSuffix");

                entity.HasIndex(e => new { e.Value, e.Name }, "IX_Product_Sku_PrefixSuffix");

                entity.HasIndex(e => new { e.Value, e.IsSuffix }, "IX_Product_Sku_PrefixSuffix_ValueType")
                    .IsUnique();

                entity.Property(e => e.Description).HasMaxLength(250);

                entity.Property(e => e.EntryDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsSuffix).HasDefaultValueSql("((0))");

                entity.Property(e => e.LastModifiedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastUsername).HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Username).HasMaxLength(50);

                entity.Property(e => e.Value).HasMaxLength(50);
            });

            modelBuilder.Entity<ProductSkuPrefixSuffixAttribute>(entity =>
            {
                entity.ToTable("Product_Sku_PrefixSuffixAttribute");

                entity.HasIndex(e => e.Name, "IX_Product_ProductSkuPrefixSuffix_Attribute_Name");

                entity.HasIndex(e => e.ProductAttributeId, "IX_Product_ProductSkuPrefixSuffix_Attribute_ProductAttributeId");

                entity.HasIndex(e => e.SkuPrefixSuffixId, "IX_Product_ProductSkuPrefixSuffix_Attribute_SkuPrefixSuffix");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Value).HasMaxLength(500);

                entity.HasOne(d => d.ProductAttribute)
                    .WithMany(p => p.ProductSkuPrefixSuffixAttributes)
                    .HasForeignKey(d => d.ProductAttributeId)
                    .HasConstraintName("FK_Product_ProductSkuPrefixSuffix_Attribute_ProductAttribute");

                entity.HasOne(d => d.SkuPrefixSuffix)
                    .WithMany(p => p.ProductSkuPrefixSuffixAttributes)
                    .HasForeignKey(d => d.SkuPrefixSuffixId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Product_ProductSkuPrefixSuffix_Attribute_ProductSkuPrefixSuffix");
            });

            modelBuilder.Entity<ProductStaticTypeReference>(entity =>
            {
                entity.ToTable("Product_StaticTypeReference");

                entity.HasIndex(e => e.Source, "IX_Product_StaticTypeReference_Source");

                entity.HasIndex(e => new { e.Source, e.Key }, "IX_Product_StaticTypeReference_SourceKey");

                entity.HasIndex(e => new { e.Source, e.Key, e.Type }, "IX_Product_StaticTypeReference_SourceKeyType")
                    .IsUnique();

                entity.Property(e => e.Key)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Source)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<ProductSupplierProduct>(entity =>
            {
                entity.HasKey(e => new { e.ProductId, e.SupplierProductId })
                    .HasName("PK__Product___4CF6A80261A30A4A");

                entity.ToTable("Product_SupplierProduct");

                entity.HasIndex(e => e.ProductId, "IX_Product_SupplierProduct_ProductId");

                entity.HasIndex(e => e.SupplierProductId, "IX_Product_SupplierProduct_SupplierProductId");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductSupplierProducts)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductSupplier_Product");

                entity.HasOne(d => d.SupplierProduct)
                    .WithMany(p => p.ProductSupplierProducts)
                    .HasForeignKey(d => d.SupplierProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Product_SupplierProduct_SupplierProduct");
            });

            modelBuilder.Entity<ProvisionMarkerDss>(entity =>
            {
                entity.HasKey(e => new { e.OwnerScopeLocalId, e.ObjectId })
                    .HasName("PK_DataSync.provision_marker_dss");

                entity.ToTable("provision_marker_dss", "DataSync");

                entity.Property(e => e.OwnerScopeLocalId).HasColumnName("owner_scope_local_id");

                entity.Property(e => e.ObjectId).HasColumnName("object_id");

                entity.Property(e => e.ProvisionDatetime)
                    .HasColumnType("datetime")
                    .HasColumnName("provision_datetime");

                entity.Property(e => e.ProvisionLocalPeerKey).HasColumnName("provision_local_peer_key");

                entity.Property(e => e.ProvisionScopeLocalId).HasColumnName("provision_scope_local_id");

                entity.Property(e => e.ProvisionScopePeerKey).HasColumnName("provision_scope_peer_key");

                entity.Property(e => e.ProvisionScopePeerTimestamp).HasColumnName("provision_scope_peer_timestamp");

                entity.Property(e => e.ProvisionTimestamp).HasColumnName("provision_timestamp");

                entity.Property(e => e.State).HasColumnName("state");

                entity.Property(e => e.Version)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("version");
            });

            modelBuilder.Entity<SchemaInfoDss>(entity =>
            {
                entity.HasKey(e => new { e.SchemaMajorVersion, e.SchemaMinorVersion })
                    .HasName("PK_DataSync.schema_info_dss");

                entity.ToTable("schema_info_dss", "DataSync");

                entity.Property(e => e.SchemaMajorVersion).HasColumnName("schema_major_version");

                entity.Property(e => e.SchemaMinorVersion).HasColumnName("schema_minor_version");

                entity.Property(e => e.SchemaExtendedInfo)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("schema_extended_info");
            });

            modelBuilder.Entity<ScopeConfigDss>(entity =>
            {
                entity.HasKey(e => e.ConfigId)
                    .HasName("PK_DataSync.scope_config_dss");

                entity.ToTable("scope_config_dss", "DataSync");

                entity.Property(e => e.ConfigId)
                    .ValueGeneratedNever()
                    .HasColumnName("config_id");

                entity.Property(e => e.ConfigData)
                    .IsRequired()
                    .HasColumnType("xml")
                    .HasColumnName("config_data");

                entity.Property(e => e.ScopeStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("scope_status")
                    .IsFixedLength(true);
            });

            modelBuilder.Entity<ScopeInfoDss>(entity =>
            {
                entity.HasKey(e => e.SyncScopeName)
                    .HasName("PK_DataSync.scope_info_dss");

                entity.ToTable("scope_info_dss", "DataSync");

                entity.Property(e => e.SyncScopeName)
                    .HasMaxLength(100)
                    .HasColumnName("sync_scope_name");

                entity.Property(e => e.ScopeConfigId).HasColumnName("scope_config_id");

                entity.Property(e => e.ScopeId)
                    .HasColumnName("scope_id")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.ScopeLocalId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("scope_local_id");

                entity.Property(e => e.ScopeRestoreCount).HasColumnName("scope_restore_count");

                entity.Property(e => e.ScopeSyncKnowledge).HasColumnName("scope_sync_knowledge");

                entity.Property(e => e.ScopeTimestamp)
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("scope_timestamp");

                entity.Property(e => e.ScopeTombstoneCleanupKnowledge).HasColumnName("scope_tombstone_cleanup_knowledge");

                entity.Property(e => e.ScopeUserComment).HasColumnName("scope_user_comment");
            });

            modelBuilder.Entity<Supplier>(entity =>
            {
                entity.ToTable("Supplier");

                entity.HasIndex(e => e.Code, "IX_Supplier_Code");

                entity.HasIndex(e => e.IsMediaTracked, "IX_Supplier_IsMediaTracked");

                entity.HasIndex(e => e.LastProductUpdate, "IX_Supplier_LastProductUpdate");

                entity.HasIndex(e => e.Name, "IX_Supplier_Name");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.IsProductFobtracked)
                    .IsRequired()
                    .HasColumnName("IsProductFOBTracked")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.IsProductTracked)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.IsPurchaseOrderTracked).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsShipmentTracked)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.IsTracked)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.LastProductUpdate).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<SupplierAttribute>(entity =>
            {
                entity.ToTable("Supplier_Attribute");

                entity.Property(e => e.Type).HasMaxLength(500);

                entity.Property(e => e.Value).HasMaxLength(500);

                entity.HasOne(d => d.Supplier)
                    .WithMany(p => p.SupplierAttributes)
                    .HasForeignKey(d => d.SupplierId)
                    .HasConstraintName("FK_SupplierAttribute_Supplier");
            });

            modelBuilder.Entity<SupplierMediaAttribute>(entity =>
            {
                entity.ToTable("Supplier_MediaAttribute");

                entity.HasIndex(e => e.Name, "IX_Supplier_MediaAttribute_Name");

                entity.HasIndex(e => e.SupplierMediaId, "IX_Supplier_MediaAttribute_SupplierMediaId");

                entity.HasIndex(e => e.Type, "IX_Supplier_MediaAttribute_Type");

                entity.HasIndex(e => new { e.Type, e.Name }, "IX_Supplier_MediaAttribute_TypeName");

                entity.HasIndex(e => new { e.Type, e.Name, e.SupplierMediaId }, "IX_Supplier_MediaAttribute_TypeNameSupplierMediaId");

                entity.HasIndex(e => e.Value, "IX_Supplier_MediaAttribute_Value");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Type).HasMaxLength(50);

                entity.Property(e => e.Value).HasMaxLength(500);

                entity.HasOne(d => d.SupplierMedia)
                    .WithMany(p => p.SupplierMediaAttributes)
                    .HasForeignKey(d => d.SupplierMediaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Supplier_MediaAttribute_Supplier_Media");
            });

            modelBuilder.Entity<SupplierMedium>(entity =>
            {
                entity.ToTable("Supplier_Media");

                entity.HasIndex(e => e.ProductId, "IX_Supplier_Media_ProductId");

                entity.HasIndex(e => e.SupplierProductId, "IX_Supplier_Media_SupplierProductId");

                entity.HasIndex(e => e.Url, "IX_Supplier_Media_Url");

                entity.Property(e => e.Color).HasMaxLength(50);

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.MediaType).HasMaxLength(50);

                entity.Property(e => e.ProductId).HasMaxLength(50);

                entity.Property(e => e.Url).HasMaxLength(500);
            });

            modelBuilder.Entity<SupplierOrder>(entity =>
            {
                entity.ToTable("Supplier_Order");

                entity.HasIndex(e => e.SupplierOrderNumber, "IX_Supplier_Order_SupplierOrderNumber");

                entity.Property(e => e.AdditionalExplanation).HasMaxLength(4000);

                entity.Property(e => e.ExpectedDeliveryDate).HasColumnType("datetime");

                entity.Property(e => e.ExpectedShipDate).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.LastModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.LastShipmentDate).HasColumnType("datetime");

                entity.Property(e => e.PurchaseOrderNumber).HasMaxLength(50);

                entity.Property(e => e.PurchaseOrderNumberOverride).HasMaxLength(50);

                entity.Property(e => e.Status).HasMaxLength(250);

                entity.Property(e => e.SupplierOrderNumber)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Supplier)
                    .WithMany(p => p.SupplierOrders)
                    .HasForeignKey(d => d.SupplierId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Supplier_Order_Supplier");
            });

            modelBuilder.Entity<SupplierOrderAddress>(entity =>
            {
                entity.ToTable("Supplier_Order_Address");

                entity.Property(e => e.City).HasMaxLength(250);

                entity.Property(e => e.Country).HasMaxLength(250);

                entity.Property(e => e.Line1)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.Line2).HasMaxLength(250);

                entity.Property(e => e.Line3).HasMaxLength(250);

                entity.Property(e => e.Line4).HasMaxLength(250);

                entity.Property(e => e.PostalCode).HasMaxLength(50);

                entity.Property(e => e.Region).HasMaxLength(250);
            });

            modelBuilder.Entity<SupplierOrderShipment>(entity =>
            {
                entity.ToTable("Supplier_Order_Shipment");

                entity.Property(e => e.Carrier).HasMaxLength(250);

                entity.Property(e => e.ShipAccount).HasMaxLength(250);

                entity.Property(e => e.ShipMethod).HasMaxLength(250);

                entity.Property(e => e.TrackingNumber).HasMaxLength(250);

                entity.HasOne(d => d.FromAddress)
                    .WithMany(p => p.SupplierOrderShipmentFromAddresses)
                    .HasForeignKey(d => d.FromAddressId)
                    .HasConstraintName("FK_Supplier_Order_Shipment_Supplier_Order_Address_From");

                entity.HasOne(d => d.SupplierOrder)
                    .WithMany(p => p.SupplierOrderShipments)
                    .HasForeignKey(d => d.SupplierOrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Supplier_Order_Shipment_Supplier_Order");

                entity.HasOne(d => d.ToAddress)
                    .WithMany(p => p.SupplierOrderShipmentToAddresses)
                    .HasForeignKey(d => d.ToAddressId)
                    .HasConstraintName("FK_Supplier_Order_Shipment_Supplier_Order_Address_To");
            });

            modelBuilder.Entity<SupplierOrderShipmentProduct>(entity =>
            {
                entity.ToTable("Supplier_Order_Shipment_Product");

                entity.Property(e => e.PurchaseOrderNumber).HasMaxLength(50);

                entity.HasOne(d => d.SupplierOrderShipment)
                    .WithMany(p => p.SupplierOrderShipmentProducts)
                    .HasForeignKey(d => d.SupplierOrderShipmentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Supplier_Order_Shipment_Item_Supplier_Order_Shipment");

                entity.HasOne(d => d.SupplierProduct)
                    .WithMany(p => p.SupplierOrderShipmentProducts)
                    .HasForeignKey(d => d.SupplierProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Supplier_Order_Shipment_Product_Supplier_Product");
            });

            modelBuilder.Entity<SupplierProduct>(entity =>
            {
                entity.ToTable("Supplier_Product");

                entity.HasIndex(e => e.Gtin, "IX_Supplier_Product_GTIN");

                entity.HasIndex(e => e.LastModifiedDate, "IX_Supplier_Product_LastModifiedDate");

                entity.HasIndex(e => e.ProductId, "IX_Supplier_Product_ProductId");

                entity.Property(e => e.Gtin)
                    .HasMaxLength(25)
                    .HasColumnName("GTIN");

                entity.Property(e => e.IsSyncEnabled)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.LastInventoryCheckDate).HasColumnType("datetime");

                entity.Property(e => e.LastModifiedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ProductBrand).HasMaxLength(250);

                entity.Property(e => e.ProductDescription).HasMaxLength(2000);

                entity.Property(e => e.ProductId)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.ProductName).HasMaxLength(500);

                entity.Property(e => e.ProductPartId).HasMaxLength(250);

                entity.Property(e => e.Size).HasMaxLength(500);

                entity.Property(e => e.Style).HasMaxLength(500);

                entity.Property(e => e.Unspsc)
                    .HasMaxLength(25)
                    .HasColumnName("UNSPSC");

                entity.HasOne(d => d.Supplier)
                    .WithMany(p => p.SupplierProducts)
                    .HasForeignKey(d => d.SupplierId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Supplier_Product_Supplier");
            });

            modelBuilder.Entity<SupplierProductAttribute>(entity =>
            {
                entity.ToTable("Supplier_Product_Attribute");

                entity.Property(e => e.Hiearchy).HasMaxLength(4000);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.Type).HasMaxLength(500);

                entity.Property(e => e.Value).HasMaxLength(2000);

                entity.HasOne(d => d.ParentAttribute)
                    .WithMany(p => p.InverseParentAttribute)
                    .HasForeignKey(d => d.ParentAttributeId)
                    .HasConstraintName("FK_Supplier_Product_Attribute_SupplierProductAttribute");
            });

            modelBuilder.Entity<SupplierProductColor>(entity =>
            {
                entity.HasKey(e => new { e.SupplierProductId, e.Style })
                    .HasName("PK__tmp_ms_x__D8846507F7F45EE6");

                entity.ToTable("Supplier_Product_Color");

                entity.Property(e => e.Style).HasMaxLength(250);

                entity.Property(e => e.Hex).HasMaxLength(50);

                entity.Property(e => e.Pmscode)
                    .HasMaxLength(250)
                    .HasColumnName("PMSCode");

                entity.HasOne(d => d.SupplierProduct)
                    .WithMany(p => p.SupplierProductColors)
                    .HasForeignKey(d => d.SupplierProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Supplier_ProductColor_Supplier_Product");
            });

            modelBuilder.Entity<SupplierProductDetail>(entity =>
            {
                entity.ToTable("Supplier_Product_Detail");

                entity.Property(e => e.LocationArray).HasMaxLength(50);

                entity.Property(e => e.ProductId).HasMaxLength(50);
            });

            modelBuilder.Entity<SupplierProductDetailFob>(entity =>
            {
                entity.ToTable("Supplier_Product_DetailFOB");

                entity.Property(e => e.FobCity)
                    .HasMaxLength(50)
                    .HasColumnName("fobCity");

                entity.Property(e => e.FobCountry)
                    .HasMaxLength(50)
                    .HasColumnName("fobCountry");

                entity.Property(e => e.FobId)
                    .HasMaxLength(50)
                    .HasColumnName("fobID");

                entity.Property(e => e.FobPostalCode)
                    .HasMaxLength(50)
                    .HasColumnName("fobPostalCode");

                entity.Property(e => e.FobState)
                    .HasMaxLength(50)
                    .HasColumnName("fobState");
            });

            modelBuilder.Entity<SupplierProductDetailFobArray>(entity =>
            {
                entity.ToTable("Supplier_Product_DetailFobArray");

                entity.Property(e => e.FobId).HasMaxLength(50);

                entity.Property(e => e.FobPostalCode).HasMaxLength(50);
            });

            modelBuilder.Entity<SupplierProductDetailFobcurrencySupported>(entity =>
            {
                entity.ToTable("Supplier_Product_DetailFOBCurrencySupported");

                entity.Property(e => e.Currency).HasColumnName("currency");

                entity.Property(e => e.SupplierProductDetailFobid).HasColumnName("SupplierProductDetailFOBId");
            });

            modelBuilder.Entity<SupplierProductDetailFobproduct>(entity =>
            {
                entity.ToTable("Supplier_Product_DetailFOBProduct");

                entity.Property(e => e.ProductId)
                    .HasMaxLength(50)
                    .HasColumnName("productId");

                entity.Property(e => e.SupplierProductDetailFobid).HasColumnName("SupplierProductDetailFOBId");
            });

            modelBuilder.Entity<SupplierProductDetailLocationArray>(entity =>
            {
                entity.ToTable("Supplier_Product_DetailLocationArray");

                entity.Property(e => e.LocationName).HasMaxLength(50);

                entity.Property(e => e.PropertyChanged).HasMaxLength(50);
            });

            modelBuilder.Entity<SupplierProductDetailLocationDecorationArray>(entity =>
            {
                entity.ToTable("Supplier_Product_DetailLocationDecorationArray");

                entity.Property(e => e.DecorationDiameter).HasColumnType("decimal(10, 4)");

                entity.Property(e => e.DecorationGeometry).HasMaxLength(50);

                entity.Property(e => e.DecorationHeight).HasColumnType("decimal(10, 4)");

                entity.Property(e => e.DecorationName).HasMaxLength(50);

                entity.Property(e => e.DecorationUnitsIncludedUom).HasMaxLength(50);

                entity.Property(e => e.DecorationWidth).HasColumnType("decimal(10, 4)");

                entity.Property(e => e.ItemPartQuantityLtmspecified).HasColumnName("ItemPartQuantityLTMSpecified");

                entity.Property(e => e.PropertyChanged).HasMaxLength(50);
            });

            modelBuilder.Entity<SupplierProductDetailLocationDecorationCharge>(entity =>
            {
                entity.ToTable("Supplier_Product_DetailLocationDecorationCharge");

                entity.Property(e => e.ChargeDescription).HasMaxLength(50);

                entity.Property(e => e.ChargeName).HasMaxLength(50);

                entity.Property(e => e.ChargesAppliesLtm).HasColumnName("ChargesAppliesLTM");

                entity.Property(e => e.ChargesPerColor).HasMaxLength(50);

                entity.Property(e => e.ChargesPerLocation).HasMaxLength(50);

                entity.Property(e => e.PropertyChanged).HasMaxLength(50);
            });

            modelBuilder.Entity<SupplierProductDetailLocationDecorationChargePrice>(entity =>
            {
                entity.ToTable("Supplier_Product_DetailLocationDecorationChargePrice");

                entity.Property(e => e.DiscountCode).HasMaxLength(50);

                entity.Property(e => e.Price).HasColumnType("decimal(10, 4)");

                entity.Property(e => e.PriceEffectiveDate).HasColumnType("datetime");

                entity.Property(e => e.PriceExpiryDate).HasColumnType("datetime");

                entity.Property(e => e.PropertyChanged).HasMaxLength(50);

                entity.Property(e => e.RepeatDiscountCode).HasMaxLength(50);

                entity.Property(e => e.RepeatPrice).HasColumnType("decimal(10, 4)");

                entity.Property(e => e.XMinQty).HasColumnName("xMinQty");

                entity.Property(e => e.XUom).HasColumnName("xUom");

                entity.Property(e => e.YMinQty).HasColumnName("yMinQty");

                entity.Property(e => e.YUom).HasColumnName("yUom");
            });

            modelBuilder.Entity<SupplierProductDetailPartArray>(entity =>
            {
                entity.ToTable("Supplier_Product_DetailPartArray");

                entity.Property(e => e.DefaultPart).HasMaxLength(50);

                entity.Property(e => e.LocationIdArray).HasMaxLength(50);

                entity.Property(e => e.PartDescription).HasMaxLength(50);

                entity.Property(e => e.PartGroupDescription).HasMaxLength(50);

                entity.Property(e => e.PartId).HasMaxLength(50);

                entity.Property(e => e.Ratio).HasColumnType("decimal(18, 2)");
            });

            modelBuilder.Entity<SupplierProductDetailPartPriceArray>(entity =>
            {
                entity.ToTable("Supplier_Product_DetailPartPriceArray");

                entity.Property(e => e.DiscountCode).HasMaxLength(50);

                entity.Property(e => e.Price).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.PriceEffectiveDate).HasColumnType("datetime");

                entity.Property(e => e.PriceExpiryDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<SupplierProductInventory>(entity =>
            {
                entity.ToTable("Supplier_Product_Inventory");

                entity.Property(e => e.AttributeFlexArray).HasMaxLength(100);

                entity.Property(e => e.AttributeSelection).HasMaxLength(100);

                entity.Property(e => e.CustomProductMessage).HasMaxLength(500);

                entity.Property(e => e.EntryType).HasMaxLength(100);

                entity.Property(e => e.LastModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PartBrand).HasMaxLength(100);

                entity.Property(e => e.PartDescription).HasMaxLength(500);

                entity.Property(e => e.PriceVariance).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Quantity).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ValidTimestamp).HasColumnType("datetime");

                entity.HasOne(d => d.SupplierProduct)
                    .WithMany(p => p.SupplierProductInventories)
                    .HasForeignKey(d => d.SupplierProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SupplierProductInventory_SupplierProduct");
            });

            modelBuilder.Entity<SupplierProductInventoryFuture>(entity =>
            {
                entity.ToTable("Supplier_Product_InventoryFuture");

                entity.HasIndex(e => e.AvailabilityOnDate, "IX_Supplier_InventoryFuture_AvaliabilityOnDate");

                entity.HasIndex(e => e.SupplierProductInventoryLocationId, "IX_Supplier_InventoryFuture_SupplierProductInventoryLocation");

                entity.Property(e => e.AvailabilityOnDate).HasColumnType("date");

                entity.HasOne(d => d.SupplierProductInventoryLocation)
                    .WithMany(p => p.SupplierProductInventoryFutures)
                    .HasForeignKey(d => d.SupplierProductInventoryLocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Supplier_InventoryFuture_SupplierInventoryLocation");
            });

            modelBuilder.Entity<SupplierProductInventoryLocation>(entity =>
            {
                entity.ToTable("Supplier_Product_InventoryLocation");

                entity.HasIndex(e => e.SupplierProductInventoryId, "IX_Supplier_Product_InventoryLocation_SupplierProductInventoryId");

                entity.Property(e => e.InventoryLocationName).HasMaxLength(100);

                entity.Property(e => e.PostalCode).HasMaxLength(10);

                entity.Property(e => e.Quantity).HasColumnType("decimal(18, 2)");

                entity.HasOne(d => d.SupplierProductInventory)
                    .WithMany(p => p.SupplierProductInventoryLocations)
                    .HasForeignKey(d => d.SupplierProductInventoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SupplierProductInventoryLocation_SupplierInventory");
            });

            modelBuilder.Entity<SupplierProductPrice>(entity =>
            {
                entity.HasKey(e => new { e.SupplierProductId, e.FobId, e.MinimumQuantity });

                entity.ToTable("Supplier_Product_Price");

                entity.Property(e => e.FobId).HasMaxLength(100);

                entity.Property(e => e.Price).HasColumnType("decimal(18, 2)");

                entity.HasOne(d => d.SupplierProduct)
                    .WithMany(p => p.SupplierProductPrices)
                    .HasForeignKey(d => d.SupplierProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Supplier_Product_Price_SupplierProduct");
            });

            modelBuilder.Entity<SupplierProductSupplierProductAttribute>(entity =>
            {
                entity.ToTable("SupplierProduct_SupplierProductAttribute");

                entity.HasOne(d => d.SupplierProductAttribute)
                    .WithMany(p => p.SupplierProductSupplierProductAttributes)
                    .HasForeignKey(d => d.SupplierProductAttributeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SupplierProduct_SupplierProductAttribute_SupplierProductAttribute");

                entity.HasOne(d => d.SupplierProduct)
                    .WithMany(p => p.SupplierProductSupplierProductAttributes)
                    .HasForeignKey(d => d.SupplierProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SupplierProduct_SupplierProductAttribute_SupplierProduct");
            });

            modelBuilder.Entity<SystemMetaProduct>(entity =>
            {
                entity.ToTable("System_Meta_Product");

                entity.Property(e => e.Mid)
                    .HasMaxLength(24)
                    .HasColumnName("MID");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.Value).HasMaxLength(4000);
            });

            modelBuilder.Entity<SystemQueue>(entity =>
            {
                entity.ToTable("System_Queue");

                entity.Property(e => e.Action)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.ActionShort).HasMaxLength(50);

                entity.Property(e => e.EntryDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Hash).HasMaxLength(2000);

                entity.Property(e => e.Metadata).HasMaxLength(2000);

                entity.Property(e => e.PushDate).HasColumnType("datetime");

                entity.Property(e => e.PushTo).HasMaxLength(50);

                entity.Property(e => e.Response).HasMaxLength(500);

                entity.Property(e => e.Source)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('QUEUED')");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.SystemQueues)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_System_Queue_QueueUser");
            });

            modelBuilder.Entity<SystemQueueUser>(entity =>
            {
                entity.ToTable("System_QueueUser");

                entity.Property(e => e.EntryDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.NotificationDate).HasColumnType("datetime");

                entity.Property(e => e.Route).HasMaxLength(50);
            });

            modelBuilder.Entity<Workflow>(entity =>
            {
                entity.ToTable("Workflow");

                entity.HasIndex(e => new { e.Name, e.Type }, "IX_Workflow_NameType")
                    .IsUnique();

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.EntryDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsActive)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.LastModifiedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastUsername).HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Username).HasMaxLength(50);
            });

            modelBuilder.Entity<WorkflowTask>(entity =>
            {
                entity.ToTable("Workflow_Task");

                entity.Property(e => e.EntryDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsActive)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.LastModifiedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastUsername).HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.RoleResponsibility).HasMaxLength(50);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Username).HasMaxLength(50);

                entity.HasOne(d => d.Workflow)
                    .WithMany(p => p.WorkflowTasks)
                    .HasForeignKey(d => d.WorkflowId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Workflow_Task_WorkFlow");
            });

            modelBuilder.Entity<WorkflowTaskAttribute>(entity =>
            {
                entity.ToTable("Workflow_TaskAttribute");

                entity.HasIndex(e => new { e.Type, e.Name }, "IX_Workflow_TaskAttribute_TypeName");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.DataType).HasMaxLength(50);

                entity.Property(e => e.EntryDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsActive)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.LastModifiedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastUsername).HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Options).HasMaxLength(500);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Username).HasMaxLength(50);

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.WorkflowTaskAttributes)
                    .HasForeignKey(d => d.TaskId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Workflow_TaskAttribute_Task");
            });

            modelBuilder.Entity<WorkflowTaskDependency>(entity =>
            {
                entity.ToTable("Workflow_TaskDependency");

                entity.HasIndex(e => e.TaskId, "IX_Workflow_TaskDependency_Task");

                entity.HasIndex(e => e.TaskDependentId, "IX_Workflow_TaskDependency_TaskDependent");

                entity.Property(e => e.EntryDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.LastModifiedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastUsername).HasMaxLength(50);

                entity.Property(e => e.Username).HasMaxLength(50);

                entity.HasOne(d => d.TaskDependent)
                    .WithMany(p => p.WorkflowTaskDependencyTaskDependents)
                    .HasForeignKey(d => d.TaskDependentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Workflow_TaskDependency_TaskDependent");

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.WorkflowTaskDependencyTasks)
                    .HasForeignKey(d => d.TaskId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Workflow_TaskDependency_Task");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
