﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class Product
    {
        public Product()
        {
            ProductCopies = new HashSet<ProductCopy>();
            ProductProductAssets = new HashSet<ProductProductAsset>();
            ProductProductAttributes = new HashSet<ProductProductAttribute>();
            ProductProductChildProducts = new HashSet<ProductProduct>();
            ProductProductParentProducts = new HashSet<ProductProduct>();
            ProductProductPrices = new HashSet<ProductProductPrice>();
            ProductProductRules = new HashSet<ProductProductRule>();
            ProductPurchaseOrders = new HashSet<ProductPurchaseOrder>();
            ProductSupplierProducts = new HashSet<ProductSupplierProduct>();
        }

        public long Id { get; set; }
        public bool? IsActive { get; set; }
        public string Sku { get; set; }
        public long? SkuId { get; set; }
        public string Mid { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public bool? IsSyncEnabled { get; set; }
        public bool IsSyncQueued { get; set; }
        public bool IsPushQueued { get; set; }
        public DateTime? LastRuleDate { get; set; }
        public DateTime? EntryDate { get; set; }
        public long? LastUserId { get; set; }
        public long? UserId { get; set; }
        public string LastUsername { get; set; }
        public string Username { get; set; }
        public DateTime? LastPimsyncDate { get; set; }
        public string Source { get; set; }

        public virtual ProductSku SkuNavigation { get; set; }
        public virtual ICollection<ProductCopy> ProductCopies { get; set; }
        public virtual ICollection<ProductProductAsset> ProductProductAssets { get; set; }
        public virtual ICollection<ProductProductAttribute> ProductProductAttributes { get; set; }
        public virtual ICollection<ProductProduct> ProductProductChildProducts { get; set; }
        public virtual ICollection<ProductProduct> ProductProductParentProducts { get; set; }
        public virtual ICollection<ProductProductPrice> ProductProductPrices { get; set; }
        public virtual ICollection<ProductProductRule> ProductProductRules { get; set; }
        public virtual ICollection<ProductPurchaseOrder> ProductPurchaseOrders { get; set; }
        public virtual ICollection<ProductSupplierProduct> ProductSupplierProducts { get; set; }
    }
}
