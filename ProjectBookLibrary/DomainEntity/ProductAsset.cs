﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class ProductAsset
    {
        public ProductAsset()
        {
            ProductProductAssetAttributes = new HashSet<ProductProductAssetAttribute>();
            ProductProductAssets = new HashSet<ProductProductAsset>();
        }

        public long Id { get; set; }
        public string S3id { get; set; }
        public string S3bucketName { get; set; }
        public string AccessLevel { get; set; }
        public string Type { get; set; }
        public string Source { get; set; }
        public string Hash { get; set; }

        public virtual ICollection<ProductProductAssetAttribute> ProductProductAssetAttributes { get; set; }
        public virtual ICollection<ProductProductAsset> ProductProductAssets { get; set; }
    }
}
