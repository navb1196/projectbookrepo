﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class ProductColor
    {
        public ProductColor()
        {
            ProductAttributes = new HashSet<ProductAttribute>();
        }

        public long Id { get; set; }
        public string Pms { get; set; }
        public string Hex { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string Name { get; set; }
        public bool? IsActive { get; set; }
        public string SwatchId { get; set; }

        public virtual ICollection<ProductAttribute> ProductAttributes { get; set; }
    }
}
