﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class ProductCopy
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public long ProductId { get; set; }
        public string Value { get; set; }

        public virtual Product Product { get; set; }
    }
}
