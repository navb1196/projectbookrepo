﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class ProductPrice
    {
        public long Id { get; set; }
        public string Type { get; set; }
        public decimal Value { get; set; }
        public decimal? Discount { get; set; }
        public string DiscountType { get; set; }
        public int? Quantity { get; set; }
        public string TypeMeta { get; set; }
    }
}
