﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class ProductProduct
    {
        public long ParentProductId { get; set; }
        public long ChildProductId { get; set; }
        public decimal Uom { get; set; }
        public string Hiearchy { get; set; }
        public string Type { get; set; }

        public virtual Product ChildProduct { get; set; }
        public virtual Product ParentProduct { get; set; }
    }
}
