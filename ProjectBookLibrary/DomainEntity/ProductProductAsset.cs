﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class ProductProductAsset
    {
        public ProductProductAsset()
        {
            ProductProductProductAttributeProductProductAssets = new HashSet<ProductProductProductAttributeProductProductAsset>();
        }

        public long Id { get; set; }
        public long ProductId { get; set; }
        public long AssetId { get; set; }
        public bool? IsInherited { get; set; }
        public bool? IsHeritable { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ProductAsset Asset { get; set; }
        public virtual Product Product { get; set; }
        public virtual ICollection<ProductProductProductAttributeProductProductAsset> ProductProductProductAttributeProductProductAssets { get; set; }
    }
}
