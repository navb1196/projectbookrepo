﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class ProductProductAttribute
    {
        public ProductProductAttribute()
        {
            ProductProductProductAttributeProductProductAssets = new HashSet<ProductProductProductAttributeProductProductAsset>();
        }

        public long Id { get; set; }
        public long ProductId { get; set; }
        public long AttributeId { get; set; }
        public long? AuditId { get; set; }

        public virtual ProductAttribute Attribute { get; set; }
        public virtual Product Product { get; set; }
        public virtual ICollection<ProductProductProductAttributeProductProductAsset> ProductProductProductAttributeProductProductAssets { get; set; }
    }
}
