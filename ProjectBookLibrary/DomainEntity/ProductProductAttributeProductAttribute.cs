﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class ProductProductAttributeProductAttribute
    {
        public long AttributeId { get; set; }
        public long AttributeIdRef { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }

        public virtual ProductAttribute Attribute { get; set; }
        public virtual ProductAttribute AttributeIdRefNavigation { get; set; }
    }
}
