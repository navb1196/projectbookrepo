﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class ProductProductAttributeProductRule
    {
        public long AttributeId { get; set; }
        public long RuleId { get; set; }

        public virtual ProductAttribute Attribute { get; set; }
        public virtual ProductRule Rule { get; set; }
    }
}
