﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class ProductProductPrice
    {
        public long Id { get; set; }
        public string Division { get; set; }
        public long ProductId { get; set; }
        public long PriceId { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? ExpireDate { get; set; }

        public virtual Product Product { get; set; }
    }
}
