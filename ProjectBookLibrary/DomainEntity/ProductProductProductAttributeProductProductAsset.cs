﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class ProductProductProductAttributeProductProductAsset
    {
        public long Id { get; set; }
        public long ProductProductAttributeId { get; set; }
        public long ProductProductAssetId { get; set; }

        public virtual ProductProductAsset ProductProductAsset { get; set; }
        public virtual ProductProductAttribute ProductProductAttribute { get; set; }
    }
}
