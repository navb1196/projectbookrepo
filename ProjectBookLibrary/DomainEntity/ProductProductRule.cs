﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class ProductProductRule
    {
        public long Id { get; set; }
        public long ProductId { get; set; }
        public long RuleId { get; set; }
        public bool Pass { get; set; }
        public string Reason { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? EntryDate { get; set; }

        public virtual Product Product { get; set; }
        public virtual ProductRule Rule { get; set; }
    }
}
