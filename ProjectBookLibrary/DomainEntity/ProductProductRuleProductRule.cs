﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class ProductProductRuleProductRule
    {
        public long Id { get; set; }
        public long RuleId { get; set; }
        public long RuleIdRef { get; set; }
        public string Type { get; set; }
    }
}
