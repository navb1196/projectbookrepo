﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class ProductPurchaseOrder
    {
        public string Ponumber { get; set; }
        public int LineNumber { get; set; }
        public long ProductId { get; set; }
        public int Total { get; set; }
        public int? Received { get; set; }
        public int Balance { get; set; }
        public string Status { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime? RequiredDate { get; set; }
        public DateTime? Available { get; set; }

        public virtual Product Product { get; set; }
    }
}
