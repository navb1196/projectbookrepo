﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class ProductRule
    {
        public ProductRule()
        {
            ProductProductAttributeProductRules = new HashSet<ProductProductAttributeProductRule>();
            ProductProductRules = new HashSet<ProductProductRule>();
        }

        public long Id { get; set; }
        public string Source { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public string Name { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? LastModifiedDate { get; set; }

        public virtual ICollection<ProductProductAttributeProductRule> ProductProductAttributeProductRules { get; set; }
        public virtual ICollection<ProductProductRule> ProductProductRules { get; set; }
    }
}
