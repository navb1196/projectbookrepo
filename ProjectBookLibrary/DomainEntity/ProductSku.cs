﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class ProductSku
    {
        public ProductSku()
        {
            Products = new HashSet<Product>();
        }

        public long Id { get; set; }
        public long? PrefixId { get; set; }
        public long? SuffixId { get; set; }
        public int? IncrementRef { get; set; }
        public string Note { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public DateTime? EntryDate { get; set; }
        public long? LastUserId { get; set; }
        public long? UserId { get; set; }
        public string LastUsername { get; set; }
        public string Username { get; set; }

        public virtual ProductSkuPrefixSuffix Prefix { get; set; }
        public virtual ProductSkuPrefixSuffix Suffix { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
