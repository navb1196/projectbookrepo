﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class ProductSkuPrefixSuffix
    {
        public ProductSkuPrefixSuffix()
        {
            ProductSkuPrefixSuffixAttributes = new HashSet<ProductSkuPrefixSuffixAttribute>();
            ProductSkuPrefixes = new HashSet<ProductSku>();
            ProductSkuSuffixes = new HashSet<ProductSku>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public bool? IsSuffix { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public DateTime? EntryDate { get; set; }
        public long? LastUserId { get; set; }
        public long? UserId { get; set; }
        public string LastUsername { get; set; }
        public string Username { get; set; }

        public virtual ICollection<ProductSkuPrefixSuffixAttribute> ProductSkuPrefixSuffixAttributes { get; set; }
        public virtual ICollection<ProductSku> ProductSkuPrefixes { get; set; }
        public virtual ICollection<ProductSku> ProductSkuSuffixes { get; set; }
    }
}
