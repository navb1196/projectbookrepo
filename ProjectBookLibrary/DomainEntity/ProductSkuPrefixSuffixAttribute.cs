﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class ProductSkuPrefixSuffixAttribute
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public long SkuPrefixSuffixId { get; set; }
        public long? ProductAttributeId { get; set; }

        public virtual ProductAttribute ProductAttribute { get; set; }
        public virtual ProductSkuPrefixSuffix SkuPrefixSuffix { get; set; }
    }
}
