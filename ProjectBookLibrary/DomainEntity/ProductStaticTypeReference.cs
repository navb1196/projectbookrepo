﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class ProductStaticTypeReference
    {
        public long Id { get; set; }
        public string Source { get; set; }
        public string Type { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
    }
}
