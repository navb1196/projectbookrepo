﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class ProductSupplierProduct
    {
        public long SupplierProductId { get; set; }
        public long ProductId { get; set; }

        public virtual Product Product { get; set; }
        public virtual SupplierProduct SupplierProduct { get; set; }
    }
}
