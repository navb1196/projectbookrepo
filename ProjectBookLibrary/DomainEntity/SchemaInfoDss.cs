﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class SchemaInfoDss
    {
        public int SchemaMajorVersion { get; set; }
        public int SchemaMinorVersion { get; set; }
        public string SchemaExtendedInfo { get; set; }
    }
}
