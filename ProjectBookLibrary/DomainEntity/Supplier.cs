﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class Supplier
    {
        public Supplier()
        {
            SupplierAttributes = new HashSet<SupplierAttribute>();
            SupplierOrders = new HashSet<SupplierOrder>();
            SupplierProducts = new HashSet<SupplierProduct>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public bool? IsTracked { get; set; }
        public bool? IsShipmentTracked { get; set; }
        public bool IsInventoryTracked { get; set; }
        public bool? IsProductTracked { get; set; }
        public DateTime? LastProductUpdate { get; set; }
        public bool IsMediaTracked { get; set; }
        public bool? IsPurchaseOrderTracked { get; set; }
        public bool? IsProductFobtracked { get; set; }

        public virtual ICollection<SupplierAttribute> SupplierAttributes { get; set; }
        public virtual ICollection<SupplierOrder> SupplierOrders { get; set; }
        public virtual ICollection<SupplierProduct> SupplierProducts { get; set; }
    }
}
