﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class SupplierAttribute
    {
        public long Id { get; set; }
        public long? SupplierId { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }

        public virtual Supplier Supplier { get; set; }
    }
}
