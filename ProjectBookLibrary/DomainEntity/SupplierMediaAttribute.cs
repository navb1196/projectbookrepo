﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class SupplierMediaAttribute
    {
        public long Id { get; set; }
        public long SupplierMediaId { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }

        public virtual SupplierMedium SupplierMedia { get; set; }
    }
}
