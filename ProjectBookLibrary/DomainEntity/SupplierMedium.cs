﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class SupplierMedium
    {
        public SupplierMedium()
        {
            SupplierMediaAttributes = new HashSet<SupplierMediaAttribute>();
        }

        public long Id { get; set; }
        public long SupplierProductId { get; set; }
        public string ProductId { get; set; }
        public string Url { get; set; }
        public string MediaType { get; set; }
        public bool? FileSizeSpecified { get; set; }
        public bool? WidthSpecified { get; set; }
        public bool? HeightSpecified { get; set; }
        public string Color { get; set; }
        public bool? DecorationIdSpecified { get; set; }
        public string Description { get; set; }
        public bool? SinglePart { get; set; }
        public bool? ChangeTimeStampSpecified { get; set; }

        public virtual ICollection<SupplierMediaAttribute> SupplierMediaAttributes { get; set; }
    }
}
