﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class SupplierOrder
    {
        public SupplierOrder()
        {
            SupplierOrderShipments = new HashSet<SupplierOrderShipment>();
        }

        public long Id { get; set; }
        public long SupplierId { get; set; }
        public string PurchaseOrderNumber { get; set; }
        public string SupplierOrderNumber { get; set; }
        public string Status { get; set; }
        public string PurchaseOrderNumberOverride { get; set; }
        public DateTime? ExpectedShipDate { get; set; }
        public DateTime? ExpectedDeliveryDate { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public DateTime? LastShipmentDate { get; set; }
        public string AdditionalExplanation { get; set; }
        public bool? IsActive { get; set; }

        public virtual Supplier Supplier { get; set; }
        public virtual ICollection<SupplierOrderShipment> SupplierOrderShipments { get; set; }
    }
}
