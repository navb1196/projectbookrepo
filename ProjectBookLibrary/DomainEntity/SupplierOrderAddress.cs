﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class SupplierOrderAddress
    {
        public SupplierOrderAddress()
        {
            SupplierOrderShipmentFromAddresses = new HashSet<SupplierOrderShipment>();
            SupplierOrderShipmentToAddresses = new HashSet<SupplierOrderShipment>();
        }

        public long Id { get; set; }
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string Line3 { get; set; }
        public string Line4 { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }

        public virtual ICollection<SupplierOrderShipment> SupplierOrderShipmentFromAddresses { get; set; }
        public virtual ICollection<SupplierOrderShipment> SupplierOrderShipmentToAddresses { get; set; }
    }
}
