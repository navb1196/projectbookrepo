﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class SupplierOrderShipment
    {
        public SupplierOrderShipment()
        {
            SupplierOrderShipmentProducts = new HashSet<SupplierOrderShipmentProduct>();
        }

        public long Id { get; set; }
        public long SupplierOrderId { get; set; }
        public long? SupplierShipId { get; set; }
        public string TrackingNumber { get; set; }
        public string Carrier { get; set; }
        public string ShipMethod { get; set; }
        public string ShipAccount { get; set; }
        public long? FromAddressId { get; set; }
        public long? ToAddressId { get; set; }

        public virtual SupplierOrderAddress FromAddress { get; set; }
        public virtual SupplierOrder SupplierOrder { get; set; }
        public virtual SupplierOrderAddress ToAddress { get; set; }
        public virtual ICollection<SupplierOrderShipmentProduct> SupplierOrderShipmentProducts { get; set; }
    }
}
