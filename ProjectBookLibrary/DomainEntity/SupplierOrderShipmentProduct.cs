﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class SupplierOrderShipmentProduct
    {
        public long Id { get; set; }
        public long SupplierOrderShipmentId { get; set; }
        public long SupplierProductId { get; set; }
        public int? Quantity { get; set; }
        public string PurchaseOrderNumber { get; set; }
        public int? PurchaseOrderLineNumber { get; set; }

        public virtual SupplierOrderShipment SupplierOrderShipment { get; set; }
        public virtual SupplierProduct SupplierProduct { get; set; }
    }
}
