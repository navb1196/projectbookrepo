﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class SupplierProduct
    {
        public SupplierProduct()
        {
            ProductSupplierProducts = new HashSet<ProductSupplierProduct>();
            SupplierOrderShipmentProducts = new HashSet<SupplierOrderShipmentProduct>();
            SupplierProductColors = new HashSet<SupplierProductColor>();
            SupplierProductInventories = new HashSet<SupplierProductInventory>();
            SupplierProductPrices = new HashSet<SupplierProductPrice>();
            SupplierProductSupplierProductAttributes = new HashSet<SupplierProductSupplierProductAttribute>();
        }

        public long Id { get; set; }
        public long SupplierId { get; set; }
        public string ProductId { get; set; }
        public string ProductPartId { get; set; }
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        public string ProductBrand { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string Style { get; set; }
        public string Size { get; set; }
        public bool IsSyncQueued { get; set; }
        public bool? IsSyncEnabled { get; set; }
        public string Gtin { get; set; }
        public string Unspsc { get; set; }
        public bool? IsCloseOut { get; set; }
        public int? IntInventory { get; set; }
        public DateTime? LastInventoryCheckDate { get; set; }

        public virtual Supplier Supplier { get; set; }
        public virtual ICollection<ProductSupplierProduct> ProductSupplierProducts { get; set; }
        public virtual ICollection<SupplierOrderShipmentProduct> SupplierOrderShipmentProducts { get; set; }
        public virtual ICollection<SupplierProductColor> SupplierProductColors { get; set; }
        public virtual ICollection<SupplierProductInventory> SupplierProductInventories { get; set; }
        public virtual ICollection<SupplierProductPrice> SupplierProductPrices { get; set; }
        public virtual ICollection<SupplierProductSupplierProductAttribute> SupplierProductSupplierProductAttributes { get; set; }
    }
}
