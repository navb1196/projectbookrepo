﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class SupplierProductAttribute
    {
        public SupplierProductAttribute()
        {
            InverseParentAttribute = new HashSet<SupplierProductAttribute>();
            SupplierProductSupplierProductAttributes = new HashSet<SupplierProductSupplierProductAttribute>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public long? ParentAttributeId { get; set; }
        public string Hiearchy { get; set; }

        public virtual SupplierProductAttribute ParentAttribute { get; set; }
        public virtual ICollection<SupplierProductAttribute> InverseParentAttribute { get; set; }
        public virtual ICollection<SupplierProductSupplierProductAttribute> SupplierProductSupplierProductAttributes { get; set; }
    }
}
