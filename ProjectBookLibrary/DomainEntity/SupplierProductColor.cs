﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class SupplierProductColor
    {
        public long SupplierProductId { get; set; }
        public string Style { get; set; }
        public string Hex { get; set; }
        public string Pmscode { get; set; }

        public virtual SupplierProduct SupplierProduct { get; set; }
    }
}
