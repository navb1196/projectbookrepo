﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class SupplierProductDetail
    {
        public long Id { get; set; }
        public long SupplierProductId { get; set; }
        public string LocationArray { get; set; }
        public string ProductId { get; set; }
        public int? Currency { get; set; }
        public int? PriceType { get; set; }
    }
}
