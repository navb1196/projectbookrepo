﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class SupplierProductDetailFob
    {
        public long Id { get; set; }
        public long SupplierProductId { get; set; }
        public string FobId { get; set; }
        public string FobCity { get; set; }
        public string FobState { get; set; }
        public string FobPostalCode { get; set; }
        public string FobCountry { get; set; }
    }
}
