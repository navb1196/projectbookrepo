﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class SupplierProductDetailFobArray
    {
        public long Id { get; set; }
        public long SupplierProductDetailId { get; set; }
        public string FobId { get; set; }
        public string FobPostalCode { get; set; }
    }
}
