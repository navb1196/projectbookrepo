﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class SupplierProductDetailFobcurrencySupported
    {
        public long Id { get; set; }
        public long SupplierProductDetailFobid { get; set; }
        public int? Currency { get; set; }
    }
}
