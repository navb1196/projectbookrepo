﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class SupplierProductDetailFobproduct
    {
        public long Id { get; set; }
        public long SupplierProductDetailFobid { get; set; }
        public string ProductId { get; set; }
    }
}
