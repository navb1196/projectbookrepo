﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class SupplierProductDetailLocationArray
    {
        public long Id { get; set; }
        public long SupplierProductDetailId { get; set; }
        public int? LocationId { get; set; }
        public string LocationName { get; set; }
        public int? DecorationsIncluded { get; set; }
        public bool? DefaultLocation { get; set; }
        public int? MaxDecoration { get; set; }
        public int? MinDecoration { get; set; }
        public int? LocationRank { get; set; }
        public string PropertyChanged { get; set; }
    }
}
