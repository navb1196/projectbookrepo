﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class SupplierProductDetailLocationDecorationArray
    {
        public long Id { get; set; }
        public long LocationId { get; set; }
        public int? DecorationId { get; set; }
        public string DecorationName { get; set; }
        public string DecorationGeometry { get; set; }
        public decimal? DecorationHeight { get; set; }
        public decimal? DecorationWidth { get; set; }
        public decimal? DecorationDiameter { get; set; }
        public int? DecorationUom { get; set; }
        public bool? AllowSubForDefaultLocation { get; set; }
        public bool? AllowSubForDefaultMethod { get; set; }
        public bool? ItemPartQuantityLtmspecified { get; set; }
        public int? DecorationUnitsIncluded { get; set; }
        public bool? DecorationUnitsIncludedSpecified { get; set; }
        public string DecorationUnitsIncludedUom { get; set; }
        public int? DecorationUnitsMax { get; set; }
        public bool? DecorationUnitsMaxSpecified { get; set; }
        public bool? DefaultDecoration { get; set; }
        public bool? LeadTimeSpecified { get; set; }
        public bool? RushLeadTimeSpecified { get; set; }
        public string PropertyChanged { get; set; }
    }
}
