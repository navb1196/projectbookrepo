﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class SupplierProductDetailLocationDecorationCharge
    {
        public long Id { get; set; }
        public long DecorationId { get; set; }
        public int? ChargeId { get; set; }
        public string ChargeName { get; set; }
        public string ChargeDescription { get; set; }
        public int? ChargeType { get; set; }
        public bool? ChargesAppliesLtm { get; set; }
        public string ChargesPerLocation { get; set; }
        public string ChargesPerColor { get; set; }
        public string PropertyChanged { get; set; }
    }
}
