﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class SupplierProductDetailLocationDecorationChargePrice
    {
        public long Id { get; set; }
        public int? ChargeId { get; set; }
        public int? XMinQty { get; set; }
        public int? XUom { get; set; }
        public int? YMinQty { get; set; }
        public int? YUom { get; set; }
        public decimal? Price { get; set; }
        public string DiscountCode { get; set; }
        public decimal? RepeatPrice { get; set; }
        public string RepeatDiscountCode { get; set; }
        public DateTime? PriceEffectiveDate { get; set; }
        public DateTime? PriceExpiryDate { get; set; }
        public string PropertyChanged { get; set; }
    }
}
