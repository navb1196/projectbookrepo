﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class SupplierProductDetailPartArray
    {
        public long Id { get; set; }
        public long SupplierProductDetailId { get; set; }
        public string PartId { get; set; }
        public string PartDescription { get; set; }
        public int? PartGroup { get; set; }
        public bool? NextPartGroupSpecified { get; set; }
        public bool? PartGroupRequired { get; set; }
        public string PartGroupDescription { get; set; }
        public decimal? Ratio { get; set; }
        public string DefaultPart { get; set; }
        public string LocationIdArray { get; set; }
    }
}
