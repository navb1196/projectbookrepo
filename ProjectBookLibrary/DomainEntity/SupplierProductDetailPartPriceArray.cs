﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class SupplierProductDetailPartPriceArray
    {
        public long Id { get; set; }
        public long SupplierProductDetailPartId { get; set; }
        public int? MinQuantity { get; set; }
        public decimal? Price { get; set; }
        public string DiscountCode { get; set; }
        public int? PriceUom { get; set; }
        public DateTime? PriceEffectiveDate { get; set; }
        public DateTime? PriceExpiryDate { get; set; }
    }
}
