﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class SupplierProductInventory
    {
        public SupplierProductInventory()
        {
            SupplierProductInventoryLocations = new HashSet<SupplierProductInventoryLocation>();
        }

        public long Id { get; set; }
        public long SupplierProductId { get; set; }
        public int? UnitOfMeasure { get; set; }
        public decimal? Quantity { get; set; }
        public bool? IsManufactured { get; set; }
        public bool? IsBuyToOrder { get; set; }
        public string AttributeSelection { get; set; }
        public string PartDescription { get; set; }
        public string PartBrand { get; set; }
        public decimal? PriceVariance { get; set; }
        public string AttributeFlexArray { get; set; }
        public string CustomProductMessage { get; set; }
        public string EntryType { get; set; }
        public DateTime? ValidTimestamp { get; set; }
        public bool? ValidTimestampSpecified { get; set; }
        public DateTime LastModifiedDate { get; set; }

        public virtual SupplierProduct SupplierProduct { get; set; }
        public virtual ICollection<SupplierProductInventoryLocation> SupplierProductInventoryLocations { get; set; }
    }
}
