﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class SupplierProductInventoryFuture
    {
        public long Id { get; set; }
        public int Quantity { get; set; }
        public DateTime? AvailabilityOnDate { get; set; }
        public int? UnitOfMeasure { get; set; }
        public long SupplierProductInventoryLocationId { get; set; }

        public virtual SupplierProductInventoryLocation SupplierProductInventoryLocation { get; set; }
    }
}
