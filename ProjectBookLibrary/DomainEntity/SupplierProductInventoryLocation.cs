﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class SupplierProductInventoryLocation
    {
        public SupplierProductInventoryLocation()
        {
            SupplierProductInventoryFutures = new HashSet<SupplierProductInventoryFuture>();
        }

        public long Id { get; set; }
        public long SupplierProductInventoryId { get; set; }
        public long InventoryLocationId { get; set; }
        public string InventoryLocationName { get; set; }
        public string PostalCode { get; set; }
        public int? Country { get; set; }
        public bool? CountrySpecified { get; set; }
        public int? UnitOfMeasure { get; set; }
        public decimal? Quantity { get; set; }

        public virtual SupplierProductInventory SupplierProductInventory { get; set; }
        public virtual ICollection<SupplierProductInventoryFuture> SupplierProductInventoryFutures { get; set; }
    }
}
