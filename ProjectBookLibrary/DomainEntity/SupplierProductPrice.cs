﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class SupplierProductPrice
    {
        public long SupplierProductId { get; set; }
        public string FobId { get; set; }
        public int MinimumQuantity { get; set; }
        public decimal? Price { get; set; }

        public virtual SupplierProduct SupplierProduct { get; set; }
    }
}
