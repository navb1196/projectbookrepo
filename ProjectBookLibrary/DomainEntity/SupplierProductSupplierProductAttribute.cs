﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class SupplierProductSupplierProductAttribute
    {
        public long Id { get; set; }
        public long SupplierProductId { get; set; }
        public long SupplierProductAttributeId { get; set; }

        public virtual SupplierProduct SupplierProduct { get; set; }
        public virtual SupplierProductAttribute SupplierProductAttribute { get; set; }
    }
}
