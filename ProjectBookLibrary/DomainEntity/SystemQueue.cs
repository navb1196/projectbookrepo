﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class SystemQueue
    {
        public long Id { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime? StartDate { get; set; }
        public long? Duration { get; set; }
        public string Status { get; set; }
        public string Response { get; set; }
        public string Source { get; set; }
        public string Action { get; set; }
        public string Metadata { get; set; }
        public string Hash { get; set; }
        public string ActionShort { get; set; }
        public long UserId { get; set; }
        public long? PushReference { get; set; }
        public string PushTo { get; set; }
        public DateTime? PushDate { get; set; }

        public virtual SystemQueueUser User { get; set; }
    }
}
