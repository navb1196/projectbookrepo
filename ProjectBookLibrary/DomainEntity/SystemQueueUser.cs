﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class SystemQueueUser
    {
        public SystemQueueUser()
        {
            SystemQueues = new HashSet<SystemQueue>();
        }

        public long Id { get; set; }
        public long UserId { get; set; }
        public DateTime EntryDate { get; set; }
        public string Route { get; set; }
        public DateTime? NotificationDate { get; set; }

        public virtual ICollection<SystemQueue> SystemQueues { get; set; }
    }
}
