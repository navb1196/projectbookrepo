﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class WorkflowTask
    {
        public WorkflowTask()
        {
            WorkflowTaskAttributes = new HashSet<WorkflowTaskAttribute>();
            WorkflowTaskDependencyTaskDependents = new HashSet<WorkflowTaskDependency>();
            WorkflowTaskDependencyTasks = new HashSet<WorkflowTaskDependency>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public bool? IsActive { get; set; }
        public bool IsComplete { get; set; }
        public long WorkflowId { get; set; }
        public DateTime? EntryDate { get; set; }
        public long? LastUserId { get; set; }
        public long? UserId { get; set; }
        public string LastUsername { get; set; }
        public string Username { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string RoleResponsibility { get; set; }
        public long? UserResposibility { get; set; }

        public virtual Workflow Workflow { get; set; }
        public virtual ICollection<WorkflowTaskAttribute> WorkflowTaskAttributes { get; set; }
        public virtual ICollection<WorkflowTaskDependency> WorkflowTaskDependencyTaskDependents { get; set; }
        public virtual ICollection<WorkflowTaskDependency> WorkflowTaskDependencyTasks { get; set; }
    }
}
