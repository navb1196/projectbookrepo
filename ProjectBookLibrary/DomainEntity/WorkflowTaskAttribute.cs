﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProjectBookLibrary.DomainEntity
{
    public partial class WorkflowTaskAttribute
    {
        public long Id { get; set; }
        public long TaskId { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string DataType { get; set; }
        public bool? IsActive { get; set; }
        public string Options { get; set; }
        public DateTime? EntryDate { get; set; }
        public long? LastUserId { get; set; }
        public long? UserId { get; set; }
        public string LastUsername { get; set; }
        public string Username { get; set; }
        public DateTime LastModifiedDate { get; set; }

        public virtual WorkflowTask Task { get; set; }
    }
}
