﻿using MediatR;
using OneSystem.Domain.Models;
using ProjectBookLibrary.DomainEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectBookLibrary.Handlers
{
   public class GetProductAttributesSearchQuery : IRequest<ProductAttribute[]>
    {
        public IEnumerable<Rule> Filter { get; set; }
        public string DistinctBy { get; set; }
        public bool Distinct { get; set; }
        public int? Start { get; set; }
        public int? Size { get; set; }
    }
}
