﻿using Microsoft.AspNetCore.Mvc;
using ProjectBookLibrary.DomainEntity;
using ProjectBookLibrary.Handlers;
using System.Threading.Tasks;

namespace ProjectBook.Controllers
{
    public class ProductController : BaseController
    {
        [HttpPost]
        public async Task<ActionResult<ProductAttribute[]>> GetProductAttributesSearch([FromBody] GetProductAttributesSearchQuery query)
        {
            return Ok(await Mediator.Send(query));
        }
    }
}
